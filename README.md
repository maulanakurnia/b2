# Assigment B2 - Intern Privy

Author : Maulana Kurnia Fiqih Ainul Yaqin

Position : Intern Backend

This repository contains A3 assignment that include:

1.[Mock Server](https://gitlab.com/maulanakurnia/b2/-/tree/main/mock-api-postman)

2.[Task Based UI](https://gitlab.com/maulanakurnia/b2/-/tree/main/task-based-ui)

3.[User Flow Diagram](https://gitlab.com/maulanakurnia/b2/-/tree/main/user-flow-diagram)
