# Task Based UI

video course : [Task Based UI](https://youtu.be/Nw-7rgEFHfI)

## Tip 1: Focus each page on one main task

![tip 1](screenshot/task-based-ui_2_3.png)

## Tip 2: Make your tasks be like commands

![tip 2](screenshot/page-title-2.png)

## Tip 3: Be mindful of your model

### a. Your model follows your business process

![tip 3](screenshot/model.png)

### b. Prevent CRUD-Based UI

![tip 3](screenshot/tip-3-crud.png)

## Tip 4: Check Your Link

### a. Minimize links to other pages not part of the main task

![tip 4](screenshot/tip-4-check-your-links.png)

### b. Keep track of links needed to complete each task

![tip 4](screenshot/tip-4-check-your-links-2.png)

### c. Make the effect of each action clear

![tip 4](screenshot/tip-4-check-your-links-2.png)

## Tip 5: Follow Established UI Patterns

![Tip 5](screenshot/tip-5-follow-established-ui-patterns.png)
