# User Flow Diagram Basics

Video Course: [User Flow Diagram Basics](https://youtu.be/cvYhuowazh0)

URL Figma: [User Flow Diagram](https://www.figma.com/file/XodI2AZ6nyZzdCmHZH0mHd/User-Flow-Basics?node-id=0%3A1&t=RZYeFQJ0s2n4z8lY-1)

## User Flow Diagram

![UFD](ufd.png)
